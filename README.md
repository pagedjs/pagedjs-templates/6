---
author: Alice Ricci
title: Guebwiller
project: paged.js sprint
book format: letter
book orientation: portrait
---



## Typefaces
### Redaction
Designed by Jeremy Mickel and Forest Young
https://www.redaction.us/
SIL Open Font License, 1.1

### Redaction
Designed by Gatti & Omnibus-Type Team
https://www.omnibus-type.com/fonts/archivo/
SIL Open Font License, 1.1





## Supported tags
See issue
